package com.epam;

import com.epam.view.FirstTaskView;
import com.epam.view.FourthView;
import com.epam.view.SecondTaskView;
import com.epam.view.ThirdTaskView;

public class ApplicationRunner {
    public static void main(String[] args) {
        new FirstTaskView().run();
        new SecondTaskView().run();
        new ThirdTaskView().run();
        new FourthView().run();

    }
}
