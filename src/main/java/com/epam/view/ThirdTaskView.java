package com.epam.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static com.epam.controller.ThirdTask.*;

public class ThirdTaskView {
    private static final Logger log = LogManager.getLogger();

    public void run(){
        List<Integer> integers = randomInteger();
        max(integers).ifPresent(value -> log.info("max is: " + value));
        min(integers).ifPresent(value -> log.info("min is: " + value));
        average(integers).ifPresent(value -> log.info("average is: " + value));
        log.info("count is: " + count(integers));
        log.info("sum is: " + sum(integers));
        log.info("sumReduce is: " + sumReduce(integers));
        log.info("countMoreThenAverage is: " + countMoreThenAverage(integers));
    }
}
