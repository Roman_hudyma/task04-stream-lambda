package com.epam.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

import static com.epam.controller.FourthTask.*;

public class FourthView {
    private static final Logger logger = LogManager.getLogger();

    public void run() {
        String text = "Some\n" +
                "text\n" +
                "to\n" +
                "split";
        Map<String, Long> stringLongMap = wordCount(text);
        Map<Character, Long> characterLongMap = charCountExceptUpperCase(text);
        logger.info("Count char in text:");
        characterLongMap.forEach((key, value) -> logger.info(key + "-" + value));
        logger.info("Count word in text:");
        stringLongMap.forEach((key, value) -> logger.info(key + "-" + value));
        logger.info("Count unique word in text:" + countUniqueWord(text));
        sortedUniqueWord(text).forEach(logger::info);

    }

}
