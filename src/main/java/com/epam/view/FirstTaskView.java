package com.epam.view;

import com.epam.controller.FirstTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FirstTaskView {

    private static final Logger log = LogManager.getLogger();
    public void run(){
        log.info("Max of 10,20,30:"+FirstTask.max().execute(10,20,30));
        log.info("Average of 10,20,30:"+FirstTask.average().execute(10,20,30));
    }
}
