package com.epam.view;

import com.epam.controller.Executor;
import com.epam.controller.second.Man;
import com.epam.model.Command;

public class SecondTaskView {
    public void run(){
    String name = "Roman";
    Executor executor = new Executor();
    Man man = new Man(name);
    //String funct1 = executor.execute(new EatImpl(man));
    String funct2 = executor.execute(new Command() {
                                         @Override
                                         public String execute() {
                                             return man.rest();
                                         }
                                     }
    );
    String funct3 =executor.execute(man::work);
    String funct4 =executor.execute(()-> name +" Sleeping...");
        System.out.println(funct3+"\n"+funct2+"\n"+funct4);
}
}
