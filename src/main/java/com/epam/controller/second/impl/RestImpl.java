package com.epam.controller.second.impl;

import com.epam.model.Command;
import com.epam.controller.second.Man;

public class RestImpl implements Command {
    private Man man;

    public RestImpl(Man man) {
        this.man = man;
    }
    @Override
    public String execute() {
        return man.rest();
    }
}
