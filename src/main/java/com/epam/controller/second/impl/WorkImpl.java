package com.epam.controller.second.impl;

import com.epam.model.Command;
import com.epam.controller.second.Man;

public class WorkImpl implements Command {
    private Man man;

    public WorkImpl(Man man) {
        this.man = man;
    }

    @Override
    public String execute() {
        return man.work();
    }
}
