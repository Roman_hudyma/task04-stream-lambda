package com.epam.controller.second.impl;

import com.epam.model.Command;
import com.epam.controller.second.Man;

public class EatImpl implements Command {
    private Man man;

    public EatImpl(Man man) {
        this.man = man;
    }

    @Override
    public String execute() {
        return man.eat();
    }
}
