package com.epam.controller;

import java.util.List;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.Random;
import java.util.stream.Collectors;

public class ThirdTask {
    public static long count(List<Integer> list) {
        return list.stream()
                .count();
    }

    public static OptionalDouble average(List<Integer> list) {
        return list.stream()
                .mapToDouble(Double::valueOf)
                .average();
    }

    public static OptionalInt min(List<Integer> list) {
        return list.stream()
                .mapToInt(Integer::intValue)
                .min();
    }

    public static OptionalInt max(List<Integer> list) {
        return list.stream()
                .mapToInt(Integer::intValue)
                .max();
    }

    public static int sum(List<Integer> list) {
        return list.stream()
                .mapToInt(Integer::intValue)
                .sum();
    }

    public static int sumReduce(List<Integer> list) {
        return list.stream()
                .mapToInt(Integer::intValue)
                .reduce(0, Integer::sum);
    }

    public static long countMoreThenAverage(List<Integer> list) {
        final double average = average(list).orElse(0);
        return list.stream()
                .filter(value -> value > average)
                .count();
    }

    public static List<Integer> randomInteger() {
        return new Random()
                .ints(10, 0, 100)
                .boxed()
                .collect(Collectors.toList());
    }
}


