package com.epam.controller;

import com.epam.model.Command;

import java.util.ArrayList;
import java.util.List;

public class Executor {
    private List<Command> function = new ArrayList<>();

    public String execute(Command funct) {
        function.add(funct);
        return funct.execute();
    }
}
