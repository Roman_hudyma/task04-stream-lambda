package com.epam.controller;

import com.epam.model.Function;

import java.util.stream.IntStream;

public class FirstTask  {
    public static Function max(){
        return (a, b, c) -> IntStream.of(a, b, c).max().orElse(0);
    }

    public static Function  average(){
        return (a, b, c) -> (int) IntStream.of(a, b, c).average().orElse(0);
    }

}
