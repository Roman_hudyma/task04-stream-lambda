package com.epam.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FourthTask {
    public static long countUniqueWord(String line){
        return Stream.of(line)
                .map(text -> text.split("\n"))
                .flatMap(Arrays::stream)
                .distinct()
                .count();
    }

    public static List<String> sortedUniqueWord(String line){
        return Stream.of(line)
                .map(text -> text.split("\n"))
                .flatMap(Arrays::stream)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    public static Map<String, Long> wordCount(String line){
        return Stream.of(line)
                .map(text -> text.split("\n"))
                .flatMap(Arrays::stream)
                .collect(Collectors.groupingBy(word -> word, Collectors.counting()));
    }

    public static Map<Character, Long> charCountExceptUpperCase(String line){
        return Stream.of(line)
                .flatMap(text -> text.codePoints().mapToObj(ch -> (char) ch))
                .filter(Character::isLowerCase)
                .collect(Collectors.groupingBy(ch -> ch, Collectors.counting()));
    }
}
