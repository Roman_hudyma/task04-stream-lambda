package com.epam.model;

@FunctionalInterface
public interface Function {
    int execute(int a,int b,int c);
}
