package com.epam.model;

@FunctionalInterface
public interface Command {
    String execute();
}
